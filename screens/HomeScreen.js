import React, { AppRegistry } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
  Dimensions,
  Button,
  CameraRoll,
  ScrollView,
  StatusBar,
  Share
} from 'react-native';
import Services from '../services/index';
import { DynamicCollage, StaticCollage } from 'react-native-images-collage';
import ImagePicker from 'react-native-image-picker';
import { LayoutData } from 'react-native-images-collage';
import ViewShot from "react-native-view-shot";
import { captureScreen } from "react-native-view-shot";
import {shareOnFacebook,shareOnTwitter,} from 'react-native-social-share';
import {AdMobBanner, AdMobInterstitial} from 'react-native-admob';


  const {width} = Dimensions.get('window');
  export default class HomeScreen extends React.Component {
    
    constructor(props) {
      super(props);
      this.ImgService = new Services.ImgService();
      this.state = {
        filePath: {},
        filePath2: {},
        filePath3: {},
        filePath4: {},
        fbImage: '',
        linkedImage: '',
        twitterImage: '',
        instaImage: '',
        loadingfbImage: false,
        loadinglinkedImage: false,
        loadingtwitterImage: false,
        loadinginstaImage: false,
        imageURI : 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/sample_img.png',
        cameraRollUri: null,
      }
    }
    
    componentDidMount() {
        const that = this;
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.setAdUnitID('ca-app-pub-6447330391732790/3870432176');

        AdMobInterstitial.addEventListener('adLoaded', () =>
          console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
          console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
          console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
          console.log('AdMobInterstitial => adClosed');
          AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
          console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.warn(error));
      
    }

    componentWillUnmount() {
      AdMobInterstitial.removeAllListeners();
    }
  
    showInterstitial() {
      AdMobInterstitial.showAd().catch(error => console.warn(error));
    }

    onClickShare() {
      Share.share(
        {
          message:
            'Hey checkout this cool new app , its called DollyPartonChallenge. https://yourgenie.app.link/BbNM9Xu8LQ',
          url: 'https://boltx.app.link/96JnvIuaA1',
          title: '#DollyPartonChallenge',
        },
        {
          // Android only:
          dialogTitle: 'Share App',
          // iOS only:
          excludedActivityTypes: ['com.apple.UIKit.activity.PostToTwitter'],
        },
      );
      
    }


    facebookShare = () => {
      shareOnFacebook({
          'text':'#DollyPartonChallenge',
          'link':'https://www.ghriantech.com/',
          'imagelink':this.state.imageURI,
          //or use image
          'image': this.state.imageURI,
        },
        (results) => {
          console.log(results);
        }
      );
    }
    captureImg = () => {
      AdMobInterstitial.showAd().catch(error => console.warn(error)),

        this.refs.viewShot.capture().then(uri => this.setState({ imageURI : uri }),
        console.warn(this.state.imageURI),
        setTimeout(() => {CameraRoll.saveToCameraRoll(this.state.imageURI,'photo')},1000),
        alert('Image Saved To Gallery'),
        error => console.error("Oops, Something Went Wrong", error)
      );
    };
    
    facebook = () => {
        var options = {
          title: 'Select Image',
    
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            this.setState({loadingfbImage: true});
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
              filePath: source,
            });
          }
        });
      };
      insta = () => {
        var options = {
          title: 'Select Image',
    
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            this.setState({loadinginstaImage: true});
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
              filePath2: source,
            });
          }
        });
      };
      linkedin = () => {
        var options = {
          title: 'Select Image',
    
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            this.setState({loadinglinkedImage: true});
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
              filePath3: source,
            });
          }
        });
      };
      twitter = () => {
        var options = {
          title: 'Select Image',
    
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            this.setState({loadingtwitterImage: true});
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
              filePath4: source,
            });
          }
        });
      };
    
    render() {
        return (
         
            <View style={styles.container}>
              <StatusBar
                barStyle="light-content"
                backgroundColor='#108C6E'
              />
               <View style={{justifyContent: 'center', alignItems: 'center',backgroundColor:'#1ceab8',height: 60}}>
                <Text
                  style={{
                    color: '#fff',
                    fontWeight: 'bold',
                    fontFamily: 'roboto',
                    fontSize: 24,
                  }}>
                  Upload Your 4 Images Below!
                </Text>
              </View>
              <View style={styles.body}>
              <View 
                style=
                  {{ 
                    zIndex: 1000, 
                    paddingTop: 5, 
                    borderTopWidth: 1, 
                    borderTopColor: '#000', 
                    position: 'absolute', 
                    width, height: 50, 
                    bottom: 0, 
                    backgroundColor: '#fff', 
                    alignItems: 'center', 
                    alignSelf: 'center', flex: 1
                  }}>
                  <AdMobBanner
                    adSize="smartBanner"
                    adUnitID="ca-app-pub-6447330391732790/7809677181"
                    didFailToReceiveAdWithError={this.bannerError}
                    onAdFailedToLoad={error => console.error(error)} />
                </View>
                {
                   <ScrollView keyboardShouldPersistTaps="always">
                     <ViewShot ref = "viewShot" options={{ format: "jpg", quality: 0.9 }}>
                         <View style={styles.card}>
                             <View style={{flexDirection: 'column'}}>
                                 <View style={{flexDirection: 'row',marginTop: 5,marginBottom: 8}}>
                                 <View>
                                         <TouchableNativeFeedback
                                             background={TouchableNativeFeedback.Ripple()}
                                             onPress={() => this.linkedin()}>
                                             <Image source={this.state.loadinglinkedImage
                                                 ? {
                                                     uri:
                                                     this.state.filePath3.uri,
                                                 }
                                                 : require('../resources/Front.jpg')} 
                                                 resizeMode = 'cover'
                                                 style={{width: width * 0.43, height: width * 0.43, marginLeft: 5,
                                                 }}/>
                                         </TouchableNativeFeedback>
                                         <View style={{alignSelf: 'center',marginTop: -38 }}>
                                             <Text style={{fontSize: 20, alignSelf: 'center',fontStyle:'italic',fontWeight: 'bold', color:'white',textShadowColor:'#333333', textShadowOffset:{width: 1, height: 1},textShadowRadius:5}}>LinkedIn</Text>
                                         </View>
                                         
                                     </View>
 
                                    <View>
                                         <TouchableNativeFeedback
                                             background={TouchableNativeFeedback.Ripple()}
                                             onPress={() => this.facebook()}>
                                             <Image source={this.state.loadingfbImage
                                                 ? {
                                                     uri:
                                                     this.state.filePath.uri,
                                                 }
                                                 : require('../resources/Front.jpg')} 
                                                 resizeMode = 'cover'
                                                 style={{width: width * 0.43, height: width * 0.43, marginLeft: 5,
                                             }}/>
                                         </TouchableNativeFeedback>
                                         <View style={{alignSelf: 'center',marginTop: -38 }}>
                                             <Text style={{fontSize: 20, alignSelf: 'center',fontStyle:'italic',fontWeight: 'bold', color:'white',textShadowColor:'#333333', textShadowOffset:{width: 1, height: 1},textShadowRadius:5}}>Facebook</Text>
                                         </View>
                                         
                                     </View>
                                     
                                    
                                 </View>
                                 <View style={{flexDirection: 'row',marginTop: 9,marginBottom: 10}}>
                                 <View>
                                         <TouchableNativeFeedback
                                             background={TouchableNativeFeedback.Ripple()}
                                             onPress={() => this.insta()}>
                                             <Image source={this.state.loadinginstaImage
                                                 ? {
                                                     uri:
                                                     this.state.filePath2.uri,
                                                 }
                                                 : require('../resources/Front.jpg')} 
                                                 resizeMode = 'cover'
                                                 style={{width: width * 0.43, height: width * 0.43, marginLeft: 5,
                                                 }}/>
                                         </TouchableNativeFeedback>
                                         <View style={{alignSelf: 'center',marginTop: -38 }}>
                                             <Text style={{fontSize: 20, alignSelf: 'center',fontStyle:'italic',fontWeight: 'bold', color:'white',textShadowColor:'#333333', textShadowOffset:{width: 1, height: 1},textShadowRadius:5}}>Instagram</Text>
                                         </View>
                                         
                                     </View>
                                     <View>
                                         <TouchableNativeFeedback
                                             background={TouchableNativeFeedback.Ripple()}
                                             onPress={() => this.twitter()}>
                                             <Image source={this.state.loadingtwitterImage
                                                 ? {
                                                     uri:
                                                     this.state.filePath4.uri,
                                                 }
                                                 : require('../resources/Front.jpg')} 
                                                 resizeMode = 'cover'
                                                 style={{width: width * 0.43, height: width * 0.43, marginLeft: 5,
                                                 }}/>
                                         </TouchableNativeFeedback>
                                         <View style={{alignSelf: 'center',marginTop: -38 }}>
                                             <Text style={{fontSize: 20, alignSelf: 'center',fontStyle:'italic',fontWeight: 'bold', color:'white',textShadowColor:'#333333', textShadowOffset:{width: 1, height: 1},textShadowRadius:5}}>Tinder</Text>
                                         </View>
                                         
                                     </View>
                                 </View>
                                 <Text style={{textAlign:'center', fontSize: 25, fontWeight:'700', fontFamily:'roboto', marginTop: 15,marginBottom: 10}}>#DollyPartonChallenge</Text>
                             </View>
                         </View>
                     </ViewShot>
                         <View style={{justifyContent:'center',alignItems:'center',marginTop:20}}>
                         <Text style= {{marginVertical:20,fontSize:18,fontStyle:'italic',color:'#777'}}>On Press Saves The Image to Gallery</Text>
                             <TouchableNativeFeedback  onPress={this.captureImg}
                                 >
                                 <View style={styles.buttonPrimary}>
                                 <Text style={{color:'white',fontSize:20,
                                   fontWeight:'bold', fontFamily:'roboto'}}>Save Image</Text>
                                 </View>
                                 </TouchableNativeFeedback>
 
                                  <TouchableNativeFeedback  onPress={this.onClickShare}
                                 >
                                 <View style={styles.buttonSecondary}>
                                 <Text style={{color:'white',fontSize:20,
                                   fontWeight:'bold', fontFamily:'roboto'}}>Share Image</Text>
                                 </View>
                                 </TouchableNativeFeedback>
                                 
                         </View>
                   </ScrollView> 
                }
                
              </View>
                
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#f2f2f2',
    },
    body: {
        flex: 1,
        padding: 20,
    },
    card: {
        backgroundColor: '#fff',
        marginTop: 40,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#eee',
      },
      buttonPrimary: {
        backgroundColor: '#1ceab8',
        color: '#fff',
        borderRadius: 2,
        paddingHorizontal: 40,
        paddingVertical: 10,
        width:'100%',
        alignItems: 'center',
        marginBottom:20
      },
      buttonSecondary: {
        backgroundColor: '#bbd0cb',
        color: '#fff',
        borderRadius: 2,
        paddingHorizontal: 40,
        paddingVertical: 10,
        width:'100%',
        alignItems: 'center',
        marginBottom:20
      },
      canvas: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    example: {
      paddingVertical: 10,
    },
    title: {
      margin: 10,
      fontSize: 20,
    },
});