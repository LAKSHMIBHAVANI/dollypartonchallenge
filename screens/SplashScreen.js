import React from 'react';
import {View, Text, Image} from 'react-native';
import {createStackNavigator} from 'react-navigation';

class SplashScreen extends React.Component {
  componentWillMount() {
    setTimeout(() => {
      // Auth: {screen: AuthNavigation};
      this.props.navigation.navigate('Welcome');
    }, 2000);
  }

  render() {
    return (
      <View style={styles.viewStyles}>
        <Image
          source={require('../resources/splash.png')}
          style={{
            height: '100%',
            resizeMode: 'contain',
            // marginTop: -5,
            // marginLeft: -2,
          }}
        />

        {/* <Text style={styles.textStyles}>
         BOLTX Comrade
        </Text>
        <Text>
            Save Money By Going Electric
        </Text> */}
      </View>
    );
  }
}

const styles = {
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF8C00',
  },
  textStyles: {
    color: 'White',
    fontSize: 30,
    fontWeight: 'bold',
  },
};

export default SplashScreen;
