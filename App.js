import React from 'react';

import {
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//import {Colors, styles} from './src/common/commonVariableStyles';
import SplashScreen from './screens/SplashScreen';
import HomeScreen from './screens/HomeScreen';

const SplashNavigation = createStackNavigator(
  {
    Home: {
      screen: SplashScreen,
    },
    Welcome: {
      screen: HomeScreen,
    },
  },
  {
    initialRouteName: 'Splash',
    defaultNavigationOptions: {
      headerStyle: {
       // backgroundColor: Colors.primaryColor,
      },
     // headerTintColor: Colors.whiteColor,
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const PrimaryNav = createSwitchNavigator(
  {
    Home: {screen: SplashScreen},
    Welcome: {screen: HomeScreen},
    //Tasks: {screen: Drawer},
    // Kyc: {screen: KycPage},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  },
);

const App = createAppContainer(PrimaryNav);

export default App;
